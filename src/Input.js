import React from 'react';
import './Input.less';
import { MdSearch } from 'react-icons/md';
import { MdKeyboardVoice } from 'react-icons/md';

const Input = () => {
  return (
    <section className='input'>
      <MdSearch className='search-icon' />
      <form action='#' method='#'>
        <input type='text' />
      </form>
      <MdKeyboardVoice className='voice-icon' />
    </section>
  );
};

export default Input;