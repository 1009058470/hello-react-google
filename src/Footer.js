import React from 'react';
import './Footer.less';

const Footer = () => {
  return (
    <section className='google-footer'>
      <section className='footer-bar'>
        <label>香港</label>
      </section>
      <section className='footer-bar'>
        <ul>
          <li><a href='https://ads.google.com/intl/zh-CN_hk/home/?subid=ww-ww-et-g-awa-a-g_hpafoot1_1!o2&utm_source=google.com&utm_medium=referral&utm_campaign=google_hpafooter&fg=1'>
            广告</a></li>
          <li><a href='https://www.google.com.hk/services/?subid=ww-ww-et-g-awa-a-g_hpbfoot1_1!o2&utm_source=google.com&utm_medium=referral&utm_campaign=google_hpbfooter&fg=1'>
            商务</a></li>
          <li><a href='https://about.google/intl/zh-CN_hk/?utm_source=google-HK&utm_medium=referral&utm_campaign=hp-footer&fg=1'>
            Google 大全</a></li>
          <li><a href='https://www.google.com/search/howsearchworks/?fg=1'>Google 搜索的运作方式</a></li>
          <li className='right'><a href='#'>设置</a></li>
          <li className='right'><a href='https://policies.google.com/terms?fg=1'>条款</a></li>
          <li className='right'><a href='https://policies.google.com/privacy?fg=1'>隐私权</a></li>
        </ul>
      </section>
    </section>
  );
};

export default Footer;