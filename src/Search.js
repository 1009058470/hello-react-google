import React from 'react';
import './Search.less';
import Input from './Input';
import Buttons from './Buttons';
import Language from './Language';

const Search = () => {
  return (
    <section className='search'>
      <header>
        <img src="https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png"
                   alt="Google Logo"/>
      </header>
      <Input />
      <Buttons/>
      <Language/>
    </section>
  );
};

export default Search;